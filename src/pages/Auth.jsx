import React from 'react';
import {Container} from "react-bootstrap";
import NavBar from "../components/NavBar";

const Auth = () => {
    return (
        <>
            <NavBar/>
            <Container>
                <h1>Auth</h1>
            </Container>
        </>
    );
};

export default Auth;