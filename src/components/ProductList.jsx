import React from 'react';
import src1 from '../assets/img/1.jpg'
import src2 from '../assets/img/2.jpg'
import src3 from '../assets/img/3.jpg'
import src4 from '../assets/img/4.jpg'
import src5 from '../assets/img/5.jpg'
import {Row} from "react-bootstrap";
import ProductItem from "./ProductItem";

const ProductList = () => {
    const product = [
        {id:1, title:'pizza five cheese', desc:'pizza five cheese is nice pizza', img: src1},
        {id:2, title:'pizza five cheese', desc:'pizza five cheese is nice pizza', img: src2},
        {id:3, title:'pizza five cheese', desc:'pizza five cheese is nice pizza', img: src3},
        {id:4, title:'pizza five cheese', desc:'pizza five cheese is nice pizza', img: src4},
        {id:5, title:'pizza five cheese', desc:'pizza five cheese is nice pizza', img: src5},
    ]
    return (
        <Row>
            {product.map(item =>
                <ProductItem
                    key={item.id}
                    prop={item}
                />
            )}
        </Row>
    );
};

export default ProductList;