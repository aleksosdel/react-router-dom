import React from 'react';
import {ListGroup} from "react-bootstrap";

const TypeBar = () => {
    const category = [
        {id:1, type: 'pizza'},
        {id:2, type: 'beverages'},
        {id:3, type: 'cheese'},
        {id:4, type: 'milk'},
        {id:5, type: 'fruit'},
    ]
    return (
        <ListGroup className='mt-4'>
            {category.map(item =>
                <ListGroup.Item key={item.id}>{item.type}</ListGroup.Item>
            )}
        </ListGroup>
    );
};

export default TypeBar;